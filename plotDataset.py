import pickle
import tensorflow as tf
import matplotlib.pyplot as plt


pickle_in = open("xtrain.pickle", "rb")
xtrs = pickle.load(pickle_in)

pickle_in = open("xtest.pickle", "rb")
xtss = pickle.load(pickle_in)

pickle_in = open("ytrain.pickle", "rb")
ytrs = pickle.load(pickle_in)

pickle_in = open("ytest.pickle", "rb")
ytss = pickle.load(pickle_in)

pickle_in =  open("animals.pickle", "rb")
animals = pickle.load(pickle_in)

model = tf.keras.models.load_model('animalRecognitionModel.keras')

pickle_in =  open("modelHistory.pickle", "rb")
hist = pickle.load(pickle_in)



plt.title('Model Accuracy')
plt.plot(hist['accuracy'],label='train')
plt.plot(hist['val_accuracy'],label='validation')
plt.legend()
plt.show()
plt.title('Model Loss')
plt.plot(hist['loss'],label='train')
plt.plot(hist['val_loss'],label='validation')
plt.legend()
plt.show()