import pickle, numpy ,math
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np

model = tf.keras.models.load_model('animalRecognitionModel.keras')


pickle_in = open("xtrain.pickle", "rb")
xtrs = pickle.load(pickle_in)

pickle_in = open("xtest.pickle", "rb")
xtss = pickle.load(pickle_in)

pickle_in = open("ytrain.pickle", "rb")
ytrs = pickle.load(pickle_in)

pickle_in = open("ytest.pickle", "rb")
ytss = pickle.load(pickle_in)

pickle_in =  open("animals.pickle", "rb")
animals = pickle.load(pickle_in)



X_train=pd.concat(xtrs,axis=0)
y_train=pd.concat(ytrs,axis=0)

X_test=pd.concat(xtss,axis=0)
y_test=pd.concat(ytss,axis=0)

train=pd.concat([X_train,y_train],axis=1)
train=train.sample(frac=1)
test=pd.concat([X_test,y_test],axis=1)
test.sample(frac=1)

X_train=train.drop('label',axis=1)
y_train=train['label']
X_test=test.drop('label',axis=1)
y_test=test['label']


shapeVal = [X_train.shape,X_test.shape,y_train.shape,y_test.shape]

print(shapeVal,type(shapeVal),shapeVal[0])

X_train=X_train.to_numpy()
X_test=X_test.to_numpy()
y_train=y_train.to_numpy()
y_test=y_test.to_numpy()

X_train=X_train.reshape(shapeVal[0][0],128,128,math.floor((shapeVal[0][1]/(128*128))))
X_test=X_test.reshape(shapeVal[1][0],128,128,math.floor((shapeVal[0][1]/(128*128))))

y_train = y_train.reshape(shapeVal[2][0],1)
y_test = y_test.reshape(shapeVal[3][0],1)

y_train=y_train.astype('int64')
y_test=y_test.astype('int64')

predictions = model.predict_step(X_test)

predictions = np.array(predictions)  # Convert predictions to numpy array

for i in predictions.argmax(axis=1):
    labels = animals[i]
    print(labels)
    plt.imshow(X_test[i])
    plt.show()