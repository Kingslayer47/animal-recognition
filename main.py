# for data processing
import matplotlib.image as mpimg
import seaborn as sns
import numpy as np
import pandas as pd
import re
import os
import glob
import cv2

# for data pipeline --------------------

from sklearn.model_selection import train_test_split
from sklearn.metrics import*
import matplotlib.pyplot as plt
from sklearn.model_selection import*
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.feature_selection import VarianceThreshold
from sklearn.pipeline import make_pipeline
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import TruncatedSVD
from skimage import data, color
from skimage.transform import rescale, resize, downscale_local_mean
import matplotlib.image as mpimg

# for prediction (machine learning models) ------------------------

from sklearn.linear_model import*
from sklearn.preprocessing import*
from sklearn.ensemble import*
from sklearn.neighbors import*
from sklearn import svm
from sklearn.naive_bayes import*
import xgboost as xgb

#deep learning libraries

import tensorflow as tf
from tensorflow.keras import Sequential

from tensorflow.keras.layers import Dense,Conv2D,MaxPool2D,Flatten,Dropout,BatchNormalization
from tensorflow.keras.optimizers import Adam

import math
print(tf.__version__)

# for confusion matrix plotting
from mlxtend.plotting import plot_confusion_matrix
from sklearn.metrics import multilabel_confusion_matrix,confusion_matrix

import pickle
import random

path = './animals/animals/'

animals = os.listdir(path)

index = 0

xtrs = []
xtss = []

ytrs = []
ytss = []

if 'xtrain.pickle' in os.listdir('./'):
    dataProcess = False if input('Do you want to re-process data? yes or no: ') == 'no' else True
else:
    dataProcess = True

if dataProcess:
    print("Starting Data Processing..")

    num = int(input("How many animals do you want to process for training dataset: "))

    animalData = []

    for _ in range(num):
        while True:
            choice = random.choice(animals)
            if choice not in animalData:
                animalData.append(choice)
                break
               
    print(f"Traning on : {animalData}")
    animals = animalData

    with open("animals.pickle", "wb") as f:
        pickle.dump(animalData, f)

    for animal in animals:
        print(f"Processing : {animal}")
        df = []
        label = []

        for i in os.listdir(path+animal):
            img = mpimg.imread(path+animal+'/'+i)
            img = resize(img,(128,128),anti_aliasing=True)
            tensor = img.reshape(49152)
            df.append(tensor)

        for _ in range(len(df)):
            label.append(index)

        index += 1

        df = pd.DataFrame(df)
        label = pd.DataFrame({'label':label})
        data_ = pd.concat([label,df], axis=1)

        X = data_.drop('label',axis=1)
        y = data_['label']

        xtr, xts, ytr, yts = train_test_split(X,y,test_size=0.1,random_state=0)

        xtrs.append(xtr)
        xtss.append(xts)

        ytrs.append(ytr)
        ytss.append(yts)


if dataProcess:
    with open("xtrain.pickle", "wb") as f:
        pickle.dump(xtrs, f)


    with open("ytrain.pickle", "wb") as f:
        pickle.dump(ytrs, f)


    with open("xtest.pickle", "wb") as f:
        pickle.dump(xtss, f)


    with open("ytest.pickle", "wb") as f:
        pickle.dump(ytss, f)


pickle_in = open("xtrain.pickle", "rb")
xtrs = pickle.load(pickle_in)

pickle_in = open("xtest.pickle", "rb")
xtss = pickle.load(pickle_in)

pickle_in = open("ytrain.pickle", "rb")
ytrs = pickle.load(pickle_in)

pickle_in = open("ytest.pickle", "rb")
ytss = pickle.load(pickle_in)


X_train=pd.concat(xtrs,axis=0)
y_train=pd.concat(ytrs,axis=0)

X_test=pd.concat(xtss,axis=0)
y_test=pd.concat(ytss,axis=0)

train=pd.concat([X_train,y_train],axis=1)
train=train.sample(frac=1)
test=pd.concat([X_test,y_test],axis=1)
test.sample(frac=1)

X_train=train.drop('label',axis=1)
y_train=train['label']
X_test=test.drop('label',axis=1)
y_test=test['label']


shapeVal = [X_train.shape,X_test.shape,y_train.shape,y_test.shape]

print(shapeVal,type(shapeVal),shapeVal[0])

X_train=X_train.to_numpy()
X_test=X_test.to_numpy()
y_train=y_train.to_numpy()
y_test=y_test.to_numpy()


X_train=X_train.reshape(shapeVal[0][0],128,128,math.floor((shapeVal[0][1]/(128*128))))
X_test=X_test.reshape(shapeVal[1][0],128,128,math.floor((shapeVal[0][1]/(128*128))))

y_train = y_train.reshape(shapeVal[2][0],1)
y_test = y_test.reshape(shapeVal[3][0],1)

y_train=y_train.astype('int64')
y_test=y_test.astype('int64')

model=Sequential()

model.add(Conv2D(64,activation='relu',kernel_size=(2,2),input_shape=X_train[0].shape))
model.add(Dense(64,activation='relu'))
model.add(Dropout(0.25))
model.add(Dense(64,activation='relu'))
model.add(Dropout(0.5))
model.add(Flatten())
model.add(Dense(128,activation='relu'))
model.add(Dense(4,activation='softmax'))

print(model.summary())

model.compile(optimizer='adam',loss='sparse_categorical_crossentropy',metrics=['accuracy'])
hist=model.fit(X_train,y_train,batch_size=20,epochs=10,verbose=1,validation_data=(X_test,y_test))
model.save('animalRecognitionModel.keras')

with open("modelHistory.pickle", "wb") as f:
    pickle.dump(hist.history, f)
